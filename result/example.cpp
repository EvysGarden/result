#include <exception>
#include <result/result.hpp>
#include <stdexcept>
#include <system_error>

using namespace result;

enum class DivisionError
{
    ZeroDivision
};

constexpr Result<int, DivisionError> divide(int a, int b) {
    if (b == 0) { return Err(DivisionError::ZeroDivision); }
    return Ok(a / b);
}

Result<int, std::error_condition> meow() {
    return Err(std::make_error_condition(std::errc::io_error));
}

Result<int, std::exception*> meow2() {
    return Err(dynamic_cast<std::exception*>(new std::runtime_error("mwrrrr")));
}

struct CustomError : public Debuggable {
    std::string m_msg;
    CustomError(std::string msg) : m_msg(msg) {}
    std::string as_debug_string() override { return m_msg; }
};

Result<int, CustomError> custom_error(int v, bool t) {
    if (t) {
        return Ok(v);
    } else {
        return Err(CustomError { "abcdefg" });
    }
}

int main() {
    static_assert(divide(2, 2) == Ok(1));
    static_assert(divide(2, 0) == Err(DivisionError::ZeroDivision));
    static_assert(divide(4, 2) == divide(8, 4));

    static_assert(divide(2, 2).also(divide(2, 0)) == Err(DivisionError::ZeroDivision));
    static_assert(divide(2, 2).also(divide(10, 2)) == Ok(5));

    // meow().value(); // would terminate
    // meow2().value(); // would terminate

    static_assert(divide(2, 2).map([](auto v) { return v - 1; }) == Ok(false));
    {
        auto       a = Ok(244).wrap<bool>();
        const auto b = a.exchange(Err(true));
        std::cout << a.value_err() << std::endl;
        std::cout << b.value() << std::endl;
    }

    {
        constexpr auto c = Err<const char*>("meow").wrap<int>();
        constexpr auto d = Ok(78).wrap<const char*>().exchange(c);
        std::cout << c.value_err() << std::endl;
        std::cout << d.value() << std::endl;
    }

    {
        constexpr auto a = Ok(222).wrap<int>();
        a.also(Ok(888).wrap<int>());
    }

    {
        auto a = custom_error(32, true).value();
        // auto b = custom_error(32, false).value(); // would terminate
    }
}