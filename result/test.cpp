#ifndef Result_TEST_CPP
#define Result_TEST_CPP

#include "result.hpp"

#include <catch2/catch_test_macros.hpp>

#include <functional>
#include <type_traits>
#include <utility>

namespace result::tests {
    using namespace result;

    TEST_CASE("creation with make functions", "[Result]") {
        SECTION("make_ok") {
            auto a = Ok(2).wrap<bool>();
            REQUIRE(a.holds_ok());
            REQUIRE_FALSE(a.holds_err());
            REQUIRE(a.value() == 2);
        }
        SECTION("make_err") {
            auto a = Err(true).wrap<int>();
            REQUIRE(a.holds_err());
            REQUIRE_FALSE(a.holds_ok());
            REQUIRE(a.value_err());
        }
    }

    TEST_CASE("creation from conversion", "[Result]") {
        SECTION("with Result::map() on ok") {
            auto a = Ok('a').wrap<int>().map<int>();
            REQUIRE(a.holds_ok());
            REQUIRE_FALSE(a.holds_err());
            REQUIRE(typeid(a.value()) == typeid(int));
            REQUIRE_FALSE(typeid(a.value()) == typeid(char));
            REQUIRE(a.value() == 97);
        }
        SECTION("with Result::map() on err") {
            auto a = Err(2).wrap<char>().map<int>();
            REQUIRE(a.holds_err());
            REQUIRE_FALSE(a.holds_ok());
            REQUIRE(typeid(a.value_err()) == typeid(int));
            REQUIRE(a.value_err() == 2);
        }
        SECTION("with Result::map(Fn&&) on ok") {
            auto a = Ok<std::string>("2").wrap<int>().map([](auto& v) { return std::stoi(v) + 8; });
            REQUIRE(a.holds_ok());
            REQUIRE_FALSE(a.holds_err());
            REQUIRE(typeid(a.value()) == typeid(int));
            REQUIRE_FALSE(typeid(a.value()) == typeid(std::string));
            REQUIRE(a.value() == 10);
        }
        SECTION("with Result::map(Fn&&) on err") {
            auto a = Err(2).wrap<std::string>().map([](auto& v) { return std::stoi(v) + 8; });
            REQUIRE(a.holds_err());
            REQUIRE_FALSE(a.holds_ok());
            REQUIRE(typeid(a.value_err()) == typeid(int));
            REQUIRE(a.value_err() == 2);
        }
        SECTION("with Result(Result<U, F>& other) on ok") {
            Result<int, int> a { Ok('a').wrap<char>() };
            REQUIRE(a.holds_ok());
            REQUIRE_FALSE(a.holds_err());
            REQUIRE(typeid(a.value()) == typeid(int));
            REQUIRE_FALSE(typeid(a.value()) == typeid(char));
            REQUIRE(a.value() == 97);
        }
        SECTION("with Result(Result<U, F>& other) on err") {
            Result<int, int> a { Err('a').wrap<char>() };
            REQUIRE(a.holds_err());
            REQUIRE_FALSE(a.holds_ok());
            REQUIRE(typeid(a.value_err()) == typeid(int));
            REQUIRE(a.value_err() == 97);
        }
    }

    TEST_CASE("equality operators", "[Result]") {
        SECTION("with Ok<U>") {
            auto a = Ok(1).wrap<char>();
            REQUIRE(a == Ok(1));
            REQUIRE_FALSE(a == Ok(2));
            REQUIRE(a == Ok(true));
            REQUIRE_FALSE(a == Ok(false));

            auto b = Err('a').wrap<int>();
            REQUIRE_FALSE(b == Ok(0));
            REQUIRE_FALSE(b == Ok(true));
        }
        SECTION("with Err<F>") {
            auto a = Err(1).wrap<char>();
            REQUIRE(a == Err(1));
            REQUIRE_FALSE(a == Err(2));
            REQUIRE(a == Err(true));
            REQUIRE_FALSE(a == Err(false));

            auto b = Ok('a').wrap<int>();
            REQUIRE_FALSE(b == Err(0));
            REQUIRE_FALSE(b == Err(true));
        }
        SECTION("with Result<U,F>") {
            auto a = Ok(1).wrap<char>();
            REQUIRE(a == Ok(1).wrap<char>());
            REQUIRE_FALSE(a == Err('a').wrap<int>());
            REQUIRE(a == Ok(true).wrap<int>());

            auto b = Err('a').wrap<int>();
            REQUIRE(b == Err('a').wrap<char>());
            REQUIRE_FALSE(b == Ok(2).wrap<int>());
            REQUIRE(b == Err(97).wrap<int>());
        }
    }

    TEST_CASE("extended getters", "[Result]") {
        SECTION("value_or(T&&)") {
            REQUIRE(Ok(2).wrap<bool>().value_or(3) == 2);
            REQUIRE(Err(false).wrap<int>().value_or(2) == 2);
        }
        SECTION("value_err_or(T&&)") {
            REQUIRE(Err(true).wrap<int>().value_err_or(false));
            REQUIRE(Ok(false).wrap<int>().value_err_or(true));
        }
        SECTION("value_or_else(Fn&&)") {
            REQUIRE(Ok(2).wrap<char>().value_or_else([] { return 3; }) == 2);
            REQUIRE(Err('a').wrap<int>().value_or_else([] { return 2; }) == 2);
        }
        SECTION("value_err_or_else(Fn&&)") {
            REQUIRE(Err('x').wrap<int>().value_err_or_else([] { return 'a'; }) == 'x');
            REQUIRE(Ok(2).wrap<char>().value_err_or_else([] { return 'c'; }) == 'c');
        }
        SECTION("value_or_default()") {
            struct A {
                int m = 2;
            };
            REQUIRE(make_ok<A>(3).wrap<char>().value_or_default().m == 3);
            REQUIRE(Err('a').wrap<A>().value_or_default().m == 2);
        }
        SECTION("value_err_or_default()") {
            struct A {
                char m = 'b';
            };
            REQUIRE(make_err<A>('a').wrap<int>().value_err_or_default().m == 'a');
            REQUIRE(Ok(2).wrap<A>().value_err_or_default().m == 'b');
        }
    }

    TEST_CASE("exchange", "[Result]") {
        SECTION("exchange return value") {
            REQUIRE(Ok(2).wrap<int>().exchange(Err(29)).value() == 2);
        }
        SECTION("mutation of old variable") {
            auto       a = Ok(2).wrap<char>();
            const auto b = a.exchange(Err('a'));
            REQUIRE(a.value_err() == 'a');
            REQUIRE(b.value() == 2);
        }
    }

    TEST_CASE("chaining with also(Result&&)", "[Result]") {
        REQUIRE(Ok(2).wrap<int>().also(Ok(3).wrap<int>()).value() == 3);
        REQUIRE(Err(2).wrap<int>().also(Ok(3).wrap<int>()).value_err() == 2);
        REQUIRE(Ok(2).wrap<int>().also(Err(3).wrap<int>()).value_err() == 3);
        REQUIRE(Err(2).wrap<int>().also(Err(3).wrap<int>()).value_err() == 2);
    }

    struct OnlyMovable final {
        int m_val = 0;
        OnlyMovable(int t_val) : m_val(t_val) {}
        OnlyMovable(const OnlyMovable&)            = delete;
        OnlyMovable& operator=(const OnlyMovable&) = delete;
        OnlyMovable(OnlyMovable&& other) noexcept : m_val(std::exchange(other.m_val, 0)) {}
        inline OnlyMovable& operator=(OnlyMovable&& other) noexcept {
            if (this != &other) { m_val = std::exchange(other.m_val, 0); }
            return *this;
        }
    };

    TEST_CASE("move-only structures", "[Result]") {
        REQUIRE(Ok(OnlyMovable(2)).wrap<bool>().value().m_val == 2);
        REQUIRE(Err(OnlyMovable(3)).wrap<bool>().value_err().m_val == 3);
    }
} // namespace result::tests

#endif