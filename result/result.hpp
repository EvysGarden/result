#ifndef RESULT_RESULT_HPP
#define RESULT_RESULT_HPP

#include <exception>
#include <functional>
#include <system_error>
#include <type_traits>
#include <utility>
#include <variant>

#include <iostream>
#include <optional>
#include <string>

namespace result {

    template<typename T, typename E>
    class Result;

    inline namespace detail {
        struct Debuggable {
            virtual std::string as_debug_string() = 0;
        };

        /**
         * @brief Ok type marker
         *
         * @tparam T type of wrapped ok value
         */
        template<typename T>
        struct Ok {
            /**
             * @brief wrapped value
             */
            T m_value;

            constexpr Ok() = default;
            constexpr Ok(T&& t_value) : m_value(std::forward<T>(t_value)) {}
            constexpr Ok(const T& t_value) : m_value(t_value) {}

            /**
             * @brief In-place ctor of `ok`
             *
             * This will construct `ok::m_value` in-place by forwarding `args` to `m_value::T{}`.
             *
             * @param args pack of arguments forwarded to `m_value::T{}`
             */
            template<typename... Args>
            constexpr explicit Ok(std::in_place_t, Args&&... args) :
                m_value { std::forward<Args>(args)... } {}

            template<typename E>
            constexpr Result<T, E> wrap() & noexcept(std::is_nothrow_copy_constructible_v<T>) {
                return Result<T, E> { *this };
            }

            template<typename E>
            constexpr Result<T, E> wrap() && noexcept(std::is_nothrow_move_constructible_v<T>) {
                return Result<T, E> { std::move(*this) };
            }
        };

        /**
         * @brief Error type marker
         *
         * @tparam T type of wrapped error value
         */
        template<typename E>
        struct Err {
            /**
             * @brief Wrapped value
             */
            E m_value;

            constexpr Err() = default;
            constexpr Err(E&& t_value) : m_value(std::forward<E>(t_value)) {}
            constexpr Err(const E& t_value) : m_value(t_value) {}

            /**
             * @brief In-place ctor of `err`
             *
             * This will construct `err::m_value` in-place by forwarding `args` to
             * `m_value::T{}`.
             *
             * @param args pack of arguments forwarded to `m_value::T{}`
             */
            template<typename... Args>
            constexpr explicit Err(std::in_place_t, Args&&... args) :
                m_value { std::forward<Args>(args)... } {}

            template<typename T>
            constexpr Result<T, E> wrap() & noexcept(std::is_nothrow_copy_constructible_v<E>) {
                return Result<T, E> { *this };
            }
            template<typename T>
            constexpr Result<T, E> wrap() && noexcept(std::is_nothrow_move_constructible_v<E>) {
                return Result<T, E> { std::move(*this) };
            }
        };
    } // namespace detail

    class bad_result_access final : std::exception {
        std::optional<std::string> m_message;

        public:
        inline bad_result_access(const std::string& t_message) noexcept : m_message(t_message) {}

        bad_result_access()          = default;
        virtual ~bad_result_access() = default;

        const char* what() const noexcept override {
            if (m_message) { return m_message->c_str(); }

            return "bad result access";
        }
    };

    /**
     * @brief Holds either `T` on success or `E` on error.
     *
     * @tparam T type of the value in case of success
     * @tparam E type of the value in case of an error
     */
    template<typename T, typename E>
    class Result final {
#if defined(RESULT_THROW_ON_EXCEPT)
#define RESULT_NOEXCEPT
#else
#define RESULT_NOEXCEPT noexcept
#endif

        std::variant<detail::Ok<T>, detail::Err<E>> m_value;

        constexpr void handle_error [[noreturn]] (const std::string& message = "") const {
#if defined(RESULT_THROW_ON_EXCEPT)
            throw bad_result_access(message);
#else
            if (std::is_constant_evaluated()) {
                throw bad_result_access(message);
            } else {
                if (!message.empty()) {
                    std::cout << "expected value not present: " << message << std::endl;
                }
                std::cout << "bad result access: ";
                if constexpr (std::is_same_v<std::remove_cvref_t<E>, std::error_condition>) {
                    detail::Err<E> err = std::get<detail::Err<E>>(m_value);
                    std::cout << err.m_value.category().name() << ": " << err.m_value.message()
                              << "(" << err.m_value.value() << ")";
                }
                if constexpr (std::is_same_v<std::remove_cvref_t<E>, std::exception>) {
                    detail::Err<E> err = std::get<detail::Err<E>>(m_value);
                    std::cout << err.m_value.what();
                }
                if constexpr (std::is_same_v<std::remove_cvref_t<E>, std::exception*>) {
                    detail::Err<E> err = std::get<detail::Err<E>>(m_value);
                    std::cout << err.m_value->what();
                }
                if constexpr (std::is_base_of_v<detail::Debuggable, std::remove_cvref_t<E>>) {
                    detail::Err<E> err = std::get<detail::Err<E>>(m_value);
                    std::cout << err.m_value.as_debug_string();
                }
                std::cout << std::endl;
                std::abort();
            }
#endif
        }

        public:
        /**
         * @brief Success value copy ctor.
         *
         * Constructs a result object by copying the ok<T>.
         * + If the copy constructor of T is a `constexpr` constructor,
         * this constructor is a `constexpr` constructor.
         */
        constexpr Result(const detail::Ok<T>& ok) noexcept(
            std::is_nothrow_copy_constructible_v<T>) :
            m_value(std::in_place_index<0>, ok) {}

        /**
         * @brief Error value copy ctor.
         *
         * Constructs a result object by copying the err<E>.
         * + If the copy constructor of E is a `constexpr` constructor,
         * this constructor is a `constexpr` constructor.
         */
        constexpr Result(const detail::Err<E>& err) noexcept(
            std::is_nothrow_copy_constructible_v<E>) :
            m_value(std::in_place_index<1>, err) {}

        /**
         * @brief Success value move ctor.
         *
         * Constructs a result object by moving the ok<T>.
         * + If the move constructor of T is a `constexpr` constructor,
         * this constructor is a `constexpr` constructor.
         */
        constexpr Result(detail::Ok<T>&& ok) noexcept(std::is_nothrow_move_constructible_v<T>) :
            m_value(std::in_place_index<0>, std::move(ok)) {}

        /**
         * @brief Error value move ctor.
         *
         * Constructs a result object by moving the err<E>.
         * + If the move constructor of E is a `constexpr` constructor,
         * this constructor is a `constexpr` constructor.
         */
        constexpr Result(detail::Err<E>&& err) noexcept(std::is_nothrow_move_constructible_v<E>) :
            m_value(std::in_place_index<1>, std::move(err)) {}

        /**
         * @brief Converting copy constructor
         *
         * If `other` contains a success value, constructs a result object that contains a success
         * value, initialized as if direct-initializing an object of type `T` with the expression
         * `other.value()`.
         *
         * If `other` contains an error value, constructs a result object that contains an error
         * value, initialized as if direct-initializing an object of type `E` with the expression
         * `other.value_err()`.
         *
         */
        // clang-format off
        template<typename U, typename F>
        requires(std::is_convertible_v<U, T>&& std::is_convertible_v<F, E>)
        constexpr Result(const Result<U, F>& other) {
            if (other.holds_ok()) {
                m_value = detail::Ok<T>(other.value());
            } else if (other.holds_err()) {
                m_value = detail::Err<E>(other.value_err());
            } else {
                handle_error();
            }
        }
        // clang-format on

        /**
         * @brief Converting move constructor
         *
         * If `other` contains a success value, constructs a result object that contains a success
         * value, initialized as if direct-initializing an object of type `T` with the expression
         * `std::move(other.value())`.
         *
         * If `other` contains an error value, constructs a result object that contains an error
         * value, initialized as if direct-initializing an object of type `E` with the expression
         * `std::move(other.value_err())`.
         *
         */
        // clang-format off
        template<typename U, typename F>
        requires(std::is_convertible_v<U, T> && std::is_convertible_v<F, E>)
        constexpr Result(Result<U, F>&& other) {
            if (other.holds_ok()) {
                m_value = detail::Ok<T>(std::move(other.value()));
            } else if (other.holds_err()) {
                m_value = detail::Err<E>(std::move(other.value_err()));
            } else {
                handle_error();
            }
        }
        // clang-format on

        constexpr bool holds_ok() const noexcept {
            return std::holds_alternative<detail::Ok<T>>(m_value);
        }

        constexpr bool holds_err() const noexcept {
            return std::holds_alternative<detail::Err<E>>(m_value);
        }

        constexpr T& value() & {
            if (holds_ok()) { return std::get<detail::Ok<T>>(m_value).m_value; }
            handle_error();
        }

        constexpr T const& value() const& {
            if (holds_ok()) { return std::get<detail::Ok<T>>(m_value).m_value; }
            handle_error();
        }

        constexpr T&& value() && {
            if (holds_ok()) { return std::move(std::get<detail::Ok<T>>(m_value).m_value); }
            handle_error();
        }

        constexpr T const&& value() const&& {
            if (holds_ok()) { return std::move(std::get<detail::Ok<T>>(m_value).m_value); }
            handle_error();
        }

        inline T& expect(const std::string& message) & RESULT_NOEXCEPT {
            if (holds_ok()) { return value(); }
            handle_error(message);
        }

        inline T&& expect(const std::string& message) && RESULT_NOEXCEPT {
            if (holds_ok()) { return std::move(value()); }
            handle_error(message);
        }

        inline T const& expect(const std::string& message) const& RESULT_NOEXCEPT {
            if (holds_ok()) { return value(); }
            handle_error(message);
        }

        inline T const&& expect(const std::string& message) const&& RESULT_NOEXCEPT {
            if (holds_ok()) { return std::move(value()); }
            handle_error(message);
        }

        constexpr T& operator*() & {
            if (holds_ok()) { return std::get<detail::Ok<T>>(m_value).m_value; }
            handle_error();
        }

        constexpr T const& operator*() const& {
            if (holds_ok()) { return std::get<detail::Ok<T>>(m_value).m_value; }
            handle_error();
        }

        constexpr T&& operator*() && {
            if (holds_ok()) { return std::move(std::get<detail::Ok<T>>(m_value).m_value); }
            handle_error();
        }

        constexpr T const&& operator*() const&& {
            if (holds_ok()) { return std::move(std::get<detail::Ok<T>>(m_value).m_value); }
            handle_error();
        }

        constexpr const T* operator->() const {
            if (holds_ok()) { return &std::get<detail::Ok<T>>(m_value).m_value; }
            handle_error();
        }

        constexpr T* operator->() {
            if (holds_ok()) { return &std::get<detail::Ok<T>>(m_value).m_value; }
            handle_error();
        }

        constexpr E& value_err() & {
            if (holds_err()) { return std::get<detail::Err<E>>(m_value).m_value; }
            handle_error();
        }

        constexpr E const& value_err() const& {
            if (holds_err()) { return std::get<detail::Err<E>>(m_value).m_value; }
            handle_error();
        }

        constexpr E&& value_err() && {
            if (holds_err()) { return std::move(std::get<detail::Err<E>>(m_value).m_value); }
            handle_error();
        }

        constexpr E const&& value_err() const&& {
            if (holds_err()) { return std::move(std::get<detail::Err<E>>(m_value).m_value); }
            handle_error();
        }

        template<typename... Args>
        constexpr T
        value_or(Args&&... args) const& noexcept(std::is_nothrow_constructible_v<T, Args...>) {
            if (holds_ok()) { return value(); }
            return { std::forward<Args>(args)... };
        }

        template<typename... Args>
        constexpr T
        value_or(Args&&... args) const&& noexcept(std::is_nothrow_constructible_v<T, Args...>) {
            if (holds_ok()) { return std::move(value()); }
            return { std::forward<Args>(args)... };
        }

        template<std::invocable Fn>
        constexpr T value_or_else(Fn&& fn) const& {
            if (holds_ok()) { return value(); }
            return std::invoke(std::forward<Fn>(fn));
        }

        template<std::invocable Fn>
        constexpr T value_or_else(Fn&& fn) const&& {
            if (holds_ok()) { return std::move(value()); }
            return std::invoke(std::forward<Fn>(fn));
        }

        constexpr T value_or_default() const& noexcept(std::is_nothrow_default_constructible_v<T>) {
            if (holds_ok()) { return value(); }
            return {};
        }

        constexpr T
        value_or_default() const&& noexcept(std::is_nothrow_default_constructible_v<T>) {
            if (holds_ok()) { return std::move(value()); }
            return {};
        }

        template<typename... Args>
        constexpr E
        value_err_or(Args&&... args) const& noexcept(std::is_nothrow_constructible_v<E, Args...>) {
            if (holds_err()) { return value_err(); }
            return { std::forward<Args>(args)... };
        }

        template<typename... Args>
        constexpr E
        value_err_or(Args&&... args) const&& noexcept(std::is_nothrow_constructible_v<E, Args...>) {
            if (holds_err()) { return std::move(value_err()); }
            return { std::forward<Args>(args)... };
        }

        template<std::invocable Fn>
        constexpr E value_err_or_else(Fn&& fn) const& {
            if (holds_err()) { return value_err(); }
            return std::invoke(std::forward<Fn>(fn));
        }

        template<std::invocable Fn>
        constexpr E value_err_or_else(Fn&& fn) const&& {
            if (holds_err()) { return std::move(value_err()); }
            return std::invoke(std::forward<Fn>(fn));
        }

        constexpr E
        value_err_or_default() const& noexcept(std::is_nothrow_default_constructible_v<E>) {
            if (holds_err()) { return value_err(); }
            return {};
        }

        constexpr E
        value_err_or_default() const&& noexcept(std::is_nothrow_default_constructible_v<E>) {
            if (holds_err()) { return std::move(value_err()); }
            return {};
        }

        /// @todo rethink r-/l-value stuff
        /// @todo make templated return `result<U,E>`
        constexpr Result& also(Result&& other) & {
            if (holds_ok()) { return other; }
            return *this;
        }

        constexpr const Result& also(Result&& other) const& {
            if (holds_ok()) { return other; }
            return *this;
        }

        constexpr Result&& also(Result&& other) && {
            if (holds_ok()) { return std::forward<Result>(other); }
            return std::move(*this);
        }

        constexpr const Result&& also(Result&& other) const&& {
            if (holds_ok()) { return std::forward<Result>(other); }
            return std::move(*this);
        }

        template<typename U>
        constexpr auto map() const& {
            return Result<U, E> { *this };
        }

        template<typename U>
        constexpr auto map() const&& {
            return Result<U, E> { std::move(*this) };
        }

        template<std::invocable<T&> Fn>
        constexpr auto map(Fn&& fn) & {
            if (holds_ok()) {
                return Result<std::remove_cvref_t<std::invoke_result_t<Fn, T&>>, E> {
                    detail::Ok<std::remove_cvref_t<std::invoke_result_t<Fn, T&>>>(
                        std::invoke(std::forward<Fn>(fn), value()))
                };
            }
            if (holds_err()) {
                return Result<std::remove_cvref_t<std::invoke_result_t<Fn, T&>>, E> {
                    detail::Err<E>(value_err())
                };
            };
            handle_error();
        }

        template<std::invocable<const T&> Fn>
        constexpr auto map(Fn&& fn) const& {
            if (holds_ok()) {
                return Result<std::remove_cvref_t<std::invoke_result_t<Fn, const T&>>, E> {
                    detail::Ok<std::remove_cvref_t<std::invoke_result_t<Fn, const T&>>>(
                        std::invoke(std::forward<Fn>(fn), value()))
                };
            }
            if (holds_err()) {
                return Result<std::remove_cvref_t<std::invoke_result_t<Fn, const T&>>, E> {
                    detail::Err<E>(value_err())
                };
            };
            handle_error();
        }

        template<std::invocable<T> Fn>
        constexpr auto map(Fn&& fn) && {
            if (holds_ok()) {
                return Result<std::remove_cvref_t<std::invoke_result_t<Fn, T>>, E> {
                    detail::Ok<std::remove_cvref_t<std::invoke_result_t<Fn, T>>>(
                        std::invoke(std::forward<Fn>(fn), std::move(value())))
                };
            }
            if (holds_err()) {
                return Result<std::remove_cvref_t<std::invoke_result_t<Fn, T>>, E> { detail::Err<E>(
                    std::move(value_err())) };
            };
            handle_error();
        }

        template<std::invocable<const T> Fn>
        constexpr auto map(Fn&& fn) const&& {
            if (holds_ok()) {
                return Result<std::remove_cvref_t<std::invoke_result_t<Fn, const T>>, E> {
                    detail::Ok<std::remove_cvref_t<std::invoke_result_t<Fn, const T>>>(
                        std::invoke(std::forward<Fn>(fn), std::move(value())))
                };
            }
            if (holds_err()) {
                return Result<std::remove_cvref_t<std::invoke_result_t<Fn, const T>>, E> {
                    detail::Err<E>(std::move(value_err()))
                };
            };
            handle_error();
        }

        template<typename R>
            requires(std::is_nothrow_convertible_v<R, Result<T, E>>)
        constexpr Result exchange(R&& other) noexcept {
            return std::exchange(*this, std::forward<R>(other));
        }

        template<typename U, typename F>
            requires(std::equality_comparable_with<T, U> && std::equality_comparable_with<E, F>)
        constexpr bool operator==(const Result<U, F>& other) const {
            if (holds_ok() && other.holds_ok()) { return value() == other.value(); }
            if (holds_err() && other.holds_err()) { return value_err() == other.value_err(); }
            return false;
        }

        template<typename U>
            requires(std::equality_comparable_with<T, U>)
        constexpr bool operator==(const Ok<U>& other) const {
            if (holds_ok()) { return value() == other.m_value; }
            return false;
        }

        template<typename F>
            requires(std::equality_comparable_with<E, F>)
        constexpr bool operator==(const Err<F>& other) const {
            if (holds_err()) { return value_err() == other.m_value; }
            return false;
        }
    };

    template<typename T, typename... Args>
    constexpr detail::Ok<T>
    make_ok(Args&&... args) noexcept(std::is_nothrow_constructible_v<T, Args...>) {
        return detail::Ok<T> { std::in_place, std::forward<Args>(args)... };
    }

    template<typename E, typename... Args>
    constexpr detail::Err<E>
    make_err(Args&&... args) noexcept(std::is_nothrow_constructible_v<E, Args...>) {
        return detail::Err<E> { std::in_place, std::forward<Args>(args)... };
    }
} // namespace result

#endif