# `Result<T, E>`

Implementation of a result type which either holds a value of type T or an error of type E