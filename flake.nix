{
  description = "A very basic flake";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, ... }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        buildInputs = with pkgs; [
          cmake
          catch2_3
          ninja
        ];
      in
      {
        packages.result = pkgs.stdenv.mkDerivation {
          name = "result";
          src = ./.;
          buildInputs = buildInputs;

          doCheck = true;
          checkPhase = ''
            runHook preCheck
            ./result_test
            runHook postCheck
          '';
        };

        packages.default = self.packages.${system}.result;
        devShell = with pkgs; mkShell {
          buildInputs = buildInputs ++ [
            clang-tools 
            ninja
            gcc
          ];
        };
      }
    );
}
